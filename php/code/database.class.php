<?php
	class Database_1 {
		private static $DB_host;
		private static $DB_name;
		private static $DB_user;
		private static $DB_password;
		
		public static function run_databaseConnection_1() {
			self::set_databaseConfig_1();
			
			$db = new PDO('mysql:host=' . self::$DB_host . ';dbname=' . self::$DB_name . ';charset=utf8', self::$DB_user, self::$DB_password);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $db;
		}
		
		
		private static function set_databaseConfig_1() {
			self::$DB_host		=	'127.0.0.1';
			self::$DB_name		=	'test_valutation';
			self::$DB_user		=	'root';
			self::$DB_password	=	'';
		}
	}