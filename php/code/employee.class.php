<?php
	class Employee_1 {
		private $id_employee;
		
		private $connection;
		
		public function set_id_employee_1($id_employee) {
			if(! is_array($id_employee)) {
				throw new Exception('Insert a valid $id_employee');
			}
			
			$this->id_employee	=	$id_employee;
		}
		
		public function get_employee_1() {
			$paramNumber	=	0;
			if(count($this->id_employee) > 0) {
				foreach ($this->id_employee as $id_employee) {
					$paramNumber	+=	1;
					$values[]						=	":$paramNumber";
					$params[":$paramNumber"]		=	$id_employee;
				}
				$whereArray[query][]				=	' id_employee IN(' . implode(', ', $values) . ') ';
			}
			
			if(count($whereArray[query]) > 0) {
				$where	=	' WHERE ' . implode(' AND ', $whereArray[query]);
			}
			
			//Eseguo la query
			try {
				require_once '../code/database.class.php';
				$conn	=	Database_1::run_databaseConnection_1();
					
				$query = "
					SELECT
						*
					FROM
						employee
					$where
				";
				$db = $conn->prepare($query);
				
				$db->execute($params);
				
				return $db->fetchAll(PDO::FETCH_ASSOC);
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		} 
	}
?>